package com.example.demo.Server;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import static com.example.demo.Server.SimpleServer.BYE;
import static com.example.demo.Server.SimpleServer.SERVER_CHARSET;

public class Chat {
    private String from;
    private String greeting;
    private Socket socket;

    public Chat(String from, String greeting, Socket socket){
        this.from=from;
        this.greeting=greeting;
        this.socket=socket;
    }

    public void chatting()throws IOException {
        Scanner in=new Scanner(System.in);
        try (
                BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream(), SERVER_CHARSET));
                PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),SERVER_CHARSET));
        ){
            System.out.println("socket连接建立成功");
            if(greeting!=null){
                pw.println("你好"+from+"."+greeting);
                pw.flush();
            }
            while (true){
                String line=br.readLine();
                if (line.trim().equalsIgnoreCase(BYE)){
                    System.out.println("对方要求断开连接");
                    pw.println(BYE);
                    pw.flush();
                    break;
                }else {
                    System.out.println("来自\""+from+"\"的消息："+line);
                }
                line=in.nextLine();
                pw.println(line);
                pw.flush();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("聊天结束");
    }
}
