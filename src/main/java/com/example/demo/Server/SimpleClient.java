package com.example.demo.Server;

import java.net.Socket;

import static com.example.demo.Server.SimpleServer.SERVER_PORT;

public class SimpleClient {
    public static void commWithServer(){
        try (
                Socket socket=new Socket("localhost",SERVER_PORT);
        ){
            Chat chat=new Chat("服务器端",null,socket);
            chat.chatting();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("程序退出");
    }

    public static void main(String[] args) {
        commWithServer();
    }
}
