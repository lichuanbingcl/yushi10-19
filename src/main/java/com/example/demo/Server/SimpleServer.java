package com.example.demo.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class SimpleServer {

    public static final int SERVER_PORT=1023;
    public static final Charset SERVER_CHARSET= StandardCharsets.UTF_8;
    public static final String BYE="bye";

    private static void commWithClient() throws IOException {
        System.out.println("SERVER端启动，在端口："+SERVER_PORT+" 监听...");
        try (
                ServerSocket s=new ServerSocket(SERVER_PORT);
                Socket ss=s.accept();
        ){
            Chat chat=new Chat("服务器端",null,ss);
            chat.chatting();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("程序退出");
    }

    public static void main(String[] args) throws IOException {
        commWithClient();
    }
}
