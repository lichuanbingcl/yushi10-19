package com.example.demo.ArrayList;

import java.util.*;

public class SetDemo1 {
    public static void main(String[] args) {
        HashSet<String> hashSet=new HashSet<>();
        LinkedHashSet<String> linkedHashSet=new LinkedHashSet<>();
        TreeSet<String> treeSet=new TreeSet<>();

        for (String s: Arrays.asList("3","1","8","5","9")){
            hashSet.add(s);
            linkedHashSet.add(s);
            treeSet.add(s);
        }
        System.out.println("HashSet:"+hashSet);
        System.out.println("LinkHashSet:"+linkedHashSet);
        System.out.println("TreeSet:"+treeSet);


        System.out.println("-------------");
        Student s1=new Student("还而过",179);
        Student s2=new Student("符合一起玩",192);
        Student s3=new Student("还而过",182);
        Student s4=new Student("发",235);

        //TreeSet<Student> treeSet1=new TreeSet<>(new MyCompareTo());//使用自然排序的方法
        TreeSet<Student> treeSet1=new TreeSet<Student>(new MyCompareTo());//使用构造器的方法进行重写
        treeSet1.add(s1);
        treeSet1.add(s2);
        treeSet1.add(s3);
        treeSet1.add(s4);

        for (Student s:treeSet1){
            System.out.println(s.getName()+"---"+s.getAge());
        }

        Iterator it=treeSet1.iterator();
        while(it.hasNext()){
            Student s= (Student) it.next();
            System.out.println(s.getName()+"---"+s.getAge());
        }
        Iterable iterable=null;
    }
}

class Student {
    private String name;
    private int age;

    public Student(String name,int age){
        this.name=name;
        this.age=age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }


//    @Override
//    public int compareTo(Student student) {
//        int num=this.name.length()-student.name.length();
//        int num1=num==0?this.name.compareTo(student.name):num;
//        int num2=num1==0?this.age-student.age:num1;
//        return num2;
//    }

}


class MyCompareTo implements Comparator<Student>{
    @Override
    public int compare(Student s1,Student s2) {
        int num=s1.getName().length()-s2.getName().length();
        int num1=num==0?s1.getName().compareTo(s2.getName()):num;
        int num2=num1==0?s1.getAge()-s2.getAge():num1;
        return num2;
    }
}