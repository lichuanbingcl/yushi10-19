package com.example.demo.ArrayList;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListDemo1 {
    public static void main(String[] args) {
        LinkedList linkedList=new LinkedList();
        ArrayList<String> list=new ArrayList<String>();
        list.add("123");
        list.add("456");
        list.add("789");
        list.add("ABC");
        list.add("EFG");
        System.out.println(list);

        System.out.println("删除指定元素，返回删除元素");
        System.out.println(list.remove(4));
        System.out.println(list);

        System.out.println("删除指定元素，返回是否成功");
        System.out.println(list.remove("123"));
        System.out.println(list);

        System.out.println("修改指定元素，返回修改元素");
        System.out.println(list.set(0,"hij"));
        System.out.println(list);

        System.out.println("返回指定索引的元素");
        System.out.println(list.get(1));

        System.out.println("返回链表的大小：");
        System.out.println(list.size());

        System.out.println("列表遍历：");
        for (Object o:list){
            System.out.println(o);
        }

        for (int i=0;i<list.size();i++){
            String s=list.get(i);
            System.out.println(s);
        }
    }
}