package com.example.demo.ArrayList;

import java.util.ArrayList;
import java.util.List;

public class ListDemo1 {
    private static List createList(){
        List retlist=new ArrayList();
        for (int i=0;i<10;i++){
            retlist.add("str"+i%5);
        }
        return retlist;
    }

    private static void getList(){
        List list=createList();
        //System.out.println(list.toString().toUpperCase());
        for (Object o:list){
            System.out.println(o.toString().toUpperCase());
        }
    }

    private static List<String> setListString(){
        List<String> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add("str"+i%5);
        }
        return list;
    }

    private static void getListString(){
        List<String> list=setListString();
        for (String s:list){
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        getList();
        getListString();
    }
}