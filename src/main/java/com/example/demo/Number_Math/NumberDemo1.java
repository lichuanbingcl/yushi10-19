package com.example.demo.Number_Math;

public class NumberDemo1 {
    public static void main(String[] args) {
        Integer integer=4;//装箱
        int n=integer;//拆箱
        Integer integer1=new Integer(4);
        Integer integer2=4;
        Integer integer3=128;
        Integer integer4=128;
        System.out.println(integer);
        System.out.println(integer1);
        System.out.println(integer==integer1);
        System.out.println(integer==integer2);
        System.out.println(integer3==integer4);

        Byte by1=127;
        Byte by2=127;
        Byte by3=63;
        Byte by4=63;
        System.out.println("------");
        System.out.println(by1==by2);
        System.out.println(by3==by4);
        System.out.println(Byte.valueOf(by1));

        Double d1=200.0;
        Double d2=200.0;
        Double d3=100.0;
        Double d4=100.0;
        System.out.println("---------");
        System.out.println(d1==d2);
        System.out.println(d3==d4);
        Double.valueOf(2);
        System.out.println(Double.valueOf(5));

        Boolean b1=true;
        Boolean b2=true;
        Boolean b3=false;
        Boolean b4=false;
        System.out.println("----------");
        System.out.println(b1==b2);
        System.out.println(b3==b4);
        System.out.println(Boolean.valueOf(true));
    }
}
