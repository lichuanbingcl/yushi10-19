package com.example.demo.Thread;

public class ThreadDemo {
    public static void main(String[] args) {
        Runnable r1=new Message("hello");
        Thread t1=new Thread(r1);
        t1.setDaemon(true);
        t1.setName("hello");
        System.out.println("start t1 thread");
        t1.start();

        Runnable r2=new Message("bye");
        Thread t2=new Thread(r2);
        t2.setPriority(Thread.MIN_PRIORITY);
        t2.setDaemon(true);
        System.out.println("start t2 thread");
        t2.start();

        System.out.println("start t3...");
        Thread t3=new GuessNumber(27);
        t3.start();
        try {
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("start t4...");
        Thread t4=new GuessNumber(74);
        t4.start();

        System.out.println("结束");

    }
}

class GuessNumber extends Thread{
    private int number;
    public GuessNumber(int number){
        this.number=number;
    }

    @Override
    public void run(){
        int counter=0;
        int guess=0;
        do {
            guess=(int)(Math.random()*100+1);
            System.out.println(this.getName()+"guess:"+guess);
            counter++;
        }while (guess!=number);
        System.out.println("** Correct!" + this.getName() + "in" + counter + "guesses.**");
    }
}

class Message implements Runnable{

    private String message;
    public Message(String message){
        this.message=message;
    }
    @Override
    public void run() {
        while (true){
            System.out.println(message);
        }
    }
}

