package com.example.demo.Thread;

import java.util.concurrent.*;

import static java.util.concurrent.Executors.*;

public class ProduceAndCustum {
    public static void main(String[] args) {
        Rescourse rescourse=new Rescourse();
        Produce produce=new Produce(rescourse);
        Thread thread=new Thread(produce,"t1");
        Thread thread1=new Thread(new Custom(rescourse),"t2");
        Thread thread2=new Thread(new Produce(rescourse),"t3");
        thread2.start();
        thread.start();
        thread1.start();
    }
}



class Rescourse{
    volatile int count=0;
    int size=10;
    public synchronized void put(){
        while(count==size){
            try {
                System.out.println("生产者进入等待");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ++count;
        System.out.println(Thread.currentThread().getName()+"生产了1个count："+count);
        notify();
    }

    public synchronized void push(){
        while (count==0){
            System.out.println("消费者进入等待");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        --count;
        System.out.println(Thread.currentThread().getName()+"消费了一个count："+count);
        notify();
    }

}

class Custom implements Runnable{
    private Rescourse rescourse;
    public Custom(Rescourse rescourse){
        this.rescourse=rescourse;
    }
    @Override
    public void run() {
        while (true){
        rescourse.push();}
    }
}

class Produce implements Runnable{
    private Rescourse rescourse;
    public Produce(Rescourse rescourse){
        this.rescourse=rescourse;
    }
    @Override
    public void run(){
        while (true){
        rescourse.put();}
    }
}