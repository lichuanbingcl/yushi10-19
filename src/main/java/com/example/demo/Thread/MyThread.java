package com.example.demo.Thread;

import java.util.ArrayList;

public class MyThread {
    public static void main(String[] args) {
        MyThread1 m=new MyThread1();
        //MyThread1 m1=new MyThread1();
        Thread t1=new Thread(m,"thread1");
        Thread t2=new Thread(m,"thread2");
        t1.start();
        t2.start();
    }
}

class MyThread1 implements Runnable{

    private final ArrayList<String> list=new ArrayList<>();
    @Override
    public synchronized void run() {
        System.out.println(Thread.currentThread().getName()+"开始运行");
        int i=5;
        while (i>0){

            try {
                list.add(Thread.currentThread().getName()+i);
                System.out.println(Thread.currentThread().getName()+" "+i);
                i--;
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(list);
    }
}
