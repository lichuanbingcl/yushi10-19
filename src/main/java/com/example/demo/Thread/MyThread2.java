package com.example.demo.Thread;

import java.util.ArrayList;

public class MyThread2 {
    public static void main(String[] args) {
        MyThread3 m=new MyThread3();
        Thread t1=new Thread(m,"thread1");
        Thread t2=new Thread(m,"thread2");
        t1.start();
        t2.start();
    }
}

class MyThread3 implements Runnable{
    private ArrayList<String> list=new ArrayList<>();

    @Override
    public void run() {
        synchronized (this){
            int i=5;
            while (i>0){
                try {
                    list.add(Thread.currentThread().getName()+i);
                    System.out.println(Thread.currentThread().getName()+" "+i);
                    i--;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(list);
        }
    }
}