package com.example.demo.Thread;


import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class BlockingQueue {
    public static void main(String[] args) {
        Res res=new Res();
        Thread t1=new Thread(new Producer(res),"t1");
        Thread t2=new Thread(new Producer(res),"t2");
        Thread t3=new Thread(new Consumer(res),"t3");
        Thread t4=new Thread(new Consumer(res),"t4");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

class Producer implements Runnable {
    private Res res;
    public Producer(Res res){
            this.res=res;
    }
    @Override
    public void run() {
        res.put();
    }
}


class Consumer implements Runnable {
    private Res res;

    public Consumer(Res res){
        this.res=res;
    }
    @Override
    public void run() {
        res.push();
    }
}

class Res{
    private final int size=10;
    private Queue<Integer> queue=new LinkedList<Integer>();

    public void put(){
        while (true){
            synchronized (queue){
                while (queue.size()==size){
                    queue.notify();
                    System.out.println("队列已满");
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                queue.add(1);
                queue.notify();
                System.out.println(Thread.currentThread().getName()+"生产一条,总量:"+queue.size());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void push(){
        while (true){
            synchronized (queue){
                while (queue.size()==0){
                    queue.notify();
                    System.out.println("队列消费完毕");
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                queue.poll();
                queue.notify();
                System.out.println(Thread.currentThread().getName()+"消费一个，总量："+queue.size());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}