package com.example.demo.Thread;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import java.util.ArrayList;
import java.util.Collection;

public class ThreadsDemo {
    public static void main(String[] args) {
        RunnablesDemo r1=new RunnablesDemo();
        Thread t1=new Thread(r1,"窗口1");
        Thread t2=new Thread(r1,"窗口2");
        Thread t3=new Thread(r1,"窗口3");
        t1.start();
        t2.start();
        t3.start();
    }
}

class RunnablesDemo implements Runnable{

    private int tickets=5;

    @Override
    public void run() {
        while (tickets>0){
            tickets--;
            System.out.println(Thread.currentThread().getName()+"卖了一张票,剩余："+tickets);
        }
    }
}

class ThreadsDemo1 extends Thread{
    private String name;
    private int tickets;
    public ThreadsDemo1(String name){
        this.name=name;
    }
    @Override
    public void run(){
        while (tickets>0){
            tickets--;
            System.out.println(name+"买了一张票，剩余："+tickets);
        }
    }
}