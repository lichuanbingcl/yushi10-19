package com.example.demo.Thread;

public class RunnableDemo implements Runnable {

    private Thread t;
    private String name;
    private String s1="1234567";
    private char[] chars=s1.toCharArray();

    public RunnableDemo(String name){
        this.name=name;
        System.out.println("创建："+this.name);
    }

//    public void start(){
//        System.out.println("start "+this.name);
//        if (t==null){
//            t=new Thread(this,this.name);
//            t.start();
//        }
//    }

    @Override
    public void run() {
        System.out.println("run"+this.name);
        try {
            for (char c:chars){
                System.out.print(c);
                Thread.sleep(1);
            }
        }catch (InterruptedException e){
            System.out.println("thread "+this.name+"interrupt");
        }
        System.out.println("thread:"+this.name+" exit");
    }
}

class RunnableDemo2 implements Runnable {

    private Thread t;
    private String name;
    private String s1="ABCDEFG";
    private char[] chars=s1.toCharArray();

    public RunnableDemo2(String name){
        this.name=name;
        System.out.println("创建："+this.name);
    }

//    public void start(){
//        System.out.println("start "+this.name);
//        if (t==null){
//            t=new Thread(this,this.name);
//            t.start();
//        }
//    }

    @Override
    public void run() {
        System.out.println("run"+this.name);
        try {
            for (char c:chars){
                System.out.print(c);
                Thread.sleep(1);
            }
        }catch (InterruptedException e){
            System.out.println("thread "+this.name+"interrupt");
        }
        System.out.println("thread:"+this.name+" exit");
    }
}

class Test{
    public static void main(String[] args) {
        RunnableDemo r1=new RunnableDemo("thread-1");
        RunnableDemo2 r2=new RunnableDemo2("thread-2");
//        r1.start();
//        r2.start();
        Thread t1=new Thread(r1);
        Thread t2=new Thread(r2);
        t1.start();
        t2.start();
    }
}
