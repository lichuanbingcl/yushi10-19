package com.example.demo.Generics;

public class GenericMethod1 {
    static class Animal{
        @Override
        public String toString(){ return "Animal"; }
    }
    static class Dog extends Animal{
        @Override
        public String toString(){ return "Dog"; }
    }
    static class Fruit{
        @Override
        public String toString(){ return "Fruit"; }
    }
    static class GenericsClass<T>{
        public void show1(T t){ System.out.println(t.toString()); }
        public <T> void show2(T t){ System.out.println(t.toString()); }
        public <K> void show3(K k){ System.out.println(k.toString()); }
    }

    public static void main(String[] args) {
        Animal animal=new Animal();
        Dog dog=new Dog();
        Fruit fruit=new Fruit();
        GenericsClass<Animal> genericsClass=new GenericsClass<>();

        //泛型类在初始化时限制了参数类型
        genericsClass.show1(animal);
        genericsClass.show1(dog);
        //genericsClass.show1(fruit);
        System.out.println("----------");
        //泛型方法的参数在使用时指定
        genericsClass.show2("12345");
        genericsClass.show2(12345);
        genericsClass.show2(animal);
        genericsClass.show2(dog);
        genericsClass.show2(fruit);
        System.out.println("----------");
        //
        genericsClass.<Animal>show3(dog);
        genericsClass.<Animal>show3(animal);
        genericsClass.show3(fruit);
        genericsClass.<Integer>show3(123);
        genericsClass.<String>show3("123");
    }
}
