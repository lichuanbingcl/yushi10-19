package com.example.demo.Generics;

import java.util.ArrayList;
import java.util.List;

public class AGeneric {
    public static void main(String[] args) {
        List list=new ArrayList();
        list.add("111");
        list.add("222");
        //list.add(new AGeneric());
        //list.add(100);
        for (int i=0;i<list.size();i++){
            String s= (String)list.get(i);
            System.out.println(s);
        }
//        for (Object o:list){
//            System.out.println(o);
//        }
    }
}
