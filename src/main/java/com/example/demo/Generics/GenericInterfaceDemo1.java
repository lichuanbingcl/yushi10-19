package com.example.demo.Generics;

//实现泛型接口方式，泛型类实现
public class GenericInterfaceDemo1<T> implements GenericInterface<T> {
    private T data;
    public void setT(T data) {
        this.data = data;
    }

    @Override
    public T getData() {
        return data;
    }

    public static void main(String[] args) {
        GenericInterfaceDemo1<String> genericInterfaceDemo1=new GenericInterfaceDemo1<>();
        genericInterfaceDemo1.setT("111,222,333");
        System.out.println("1:"+genericInterfaceDemo1.getData());
    }
}

//实现泛型接口方式二，指定具体类型
class GenericInterfaceDemo2 implements GenericInterface<String>{

    private String data;

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String getData() {
        return data;
    }

    public static void main(String[] args) {
        GenericInterfaceDemo2 genericInterfaceDemo2=new GenericInterfaceDemo2();
        genericInterfaceDemo2.setData("111,222,333");
        System.out.println("2:"+genericInterfaceDemo2.getData());
    }
}

interface GenericInterface<T>{
    T getData();
}
