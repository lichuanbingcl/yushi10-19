package com.example.demo.Generics;

public class TypeLimitForMethod {
//    private static <T> T getMin(T a,T b){
//        return (a.compareTo(b)<0)?a:b;
//    }//使用时没有限定类型，无法获取到方法

    public static <T extends Comparable<T>>T getMin(T a,T b){
        return (a.compareTo(b)<0)?a:b;
    }

    public static void main(String[] args) {
        System.out.println(getMin(2,4));
        System.out.println(getMin("a","b"));
        System.out.println(getMin('a','b'));
        System.out.println(getMin(true,false));
    }
}
