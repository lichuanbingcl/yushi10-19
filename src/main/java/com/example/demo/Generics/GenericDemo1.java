package com.example.demo.Generics;

public class GenericDemo1<T> {
    private static int add(int a,int b){
        System.out.println(a+"+"+b+"="+(a+b));
        return a+b;
    }

    private static double add(double a,double b){
        System.out.println(a+"+"+b+"="+(a+b));
        return a+b;
    }

    private static float add(float a,float b){
        System.out.println(a+"+"+b+"="+(a+b));
        return a+b;
    }

    private static <T extends Number> double add(T a,T b){
        System.out.println(a+"+"+b+"="+(a.doubleValue()+b.doubleValue()));
        return a.doubleValue()+b.doubleValue();
    }

    public static void main(String[] args) {
        GenericDemo1.add(1,2);
        GenericDemo1.add(1f,2f);
        GenericDemo1.add(1d,2d);
        GenericDemo1.add(Integer.valueOf(1),Integer.valueOf(2));
        GenericDemo1.add(Character.valueOf('1'),Character.valueOf('2'));
        GenericDemo1.add(Double.valueOf(1),Double.valueOf(2));
        GenericDemo1.add(Float.valueOf(1),Float.valueOf(2));
        GenericDemo1.add(Long.valueOf(1),Long.valueOf(2));

        GenericDemo1<Integer> integerGenericDemo1=new GenericDemo1<Integer>();
        GenericDemo1<Integer> integerGenericDemo11=new GenericDemo1<>();
        GenericDemo1<String> stringGenericDemo1=new GenericDemo1<String>();
        System.out.println(integerGenericDemo1==integerGenericDemo11);
        System.out.println(integerGenericDemo1==integerGenericDemo1);
        System.out.println(integerGenericDemo1.getClass()==stringGenericDemo1.getClass());
        System.out.println(integerGenericDemo1.getClass());
        System.out.println(stringGenericDemo1.getClass());

        System.out.println("----");
        GenericDemo1<String>[] genericDemo1s=new GenericDemo1[10];
    }
}
