package com.example.demo.Generics;

import java.util.ArrayList;

public class GenericClass <T>{
    private T data;
    private T getData(){
        return data;
    }

    private void setData(T data) {
        this.data = data;
    }


    public static void main(String[] args) {
        GenericClass<String> genericClass=new GenericClass<>();
        genericClass.setData("123,456,789");
        System.out.println(genericClass.getData());
    }
}
