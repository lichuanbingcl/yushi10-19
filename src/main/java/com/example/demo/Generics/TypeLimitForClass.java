package com.example.demo.Generics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TypeLimitForClass <T extends List & Serializable> {
    public T data;
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }

    private static<T extends Comparable<T>> T getMin(T a,T b){
        return (a.compareTo(b))>0?a:b;
    }

    public static void main(String[] args) {
        ArrayList<String> stringArrayList=new ArrayList<>();
        stringArrayList.add("A");
        stringArrayList.add("B");
        ArrayList<Integer> integerArrayList=new ArrayList<>();
        integerArrayList.add(1);
        integerArrayList.add(2);
        integerArrayList.add(3);
        TypeLimitForClass<ArrayList> typeLimitForClass=new TypeLimitForClass<>();
        typeLimitForClass.setData(stringArrayList);
        TypeLimitForClass<ArrayList> typeLimitForClass1=new TypeLimitForClass<>();
        typeLimitForClass1.setData(integerArrayList);
        System.out.println(getMin(typeLimitForClass.getData().size(),typeLimitForClass1.getData().size()));

        System.out.println(typeLimitForClass==typeLimitForClass1);
        System.out.println(typeLimitForClass==typeLimitForClass);
    }
}
