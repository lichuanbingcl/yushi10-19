package com.example.demo.lambda;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LambdaDemo {
    public static void main(String[] args) throws UnknownHostException {
        ServerSocket s=null;
        Socket ss;
        InetAddress address=InetAddress.getByName("www.geekbang.com");
        System.out.println(address);
        Map map=new HashMap();
        map.put(123,231);
        map.put(1234,2134);
        System.out.println(map);
        System.out.println(map.containsKey(123));
        System.out.println(map.get(123));
        System.out.println(map.remove(1234));
    }
}
