package com.example.demo.Exception;

public class ExceptionDemo {
    public static void main(String[] args) {


        try (
                AutoCloseRes autoCloseRes=new AutoCloseRes("res1");
                AutoCloseRes autoCloseRes2 = new AutoCloseRes("res2")) {
            while (true){
                System.out.println(autoCloseRes.read());
                System.out.println(autoCloseRes2.read());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
