package com.example.demo.Exception;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 文本记录日志
 * 使用子线程写入日志；添加异常日志记录
 */
public class LogUtil {
    private static String log_path=getBasePath();//日志保存路径
    private static String log_name="exp_log";//日志名
    private static boolean console_out=true;//是否输出到控制台
    /*
     * yyyy-MM: 每个月更新一个log日志 yyyy-ww: 每个星期更新一个log日志 yyyy-MM-dd: 每天更新一个log日志
     * yyyy-MM-dd-a: 每天的午夜和正午更新一个log日志 yyyy-MM-dd-HH: 每小时更新一个log日志
     * yyyy-MM-dd-HH-mm: 每分钟更新一个log日志
     */
    private static String update_hz="yyyy-MM-dd";//更新频率，每天
    private static long max_long_siaz=1024*1024*10;//单个日志文件大小10M

    public static void debug(String msg){
        runWrite(msg,log_path,log_name+"_debug");
    }

    public static void info(String msg){
        runWrite(msg,log_path,log_name+"_info");
    }
    public static void error(String msg){
        runWrite(msg,log_path,log_name+"_error");
    }

    public static void exception(Exception e){
        String errorMessage=e.getMessage()+"";
        StackTraceElement[] eArray=e.getCause().getStackTrace();
        for (int i=0;i<eArray.length;i++){
            String className=e.getCause().getStackTrace()[i].getClassName();
            String MethodName=e.getCause().getStackTrace()[i].getMethodName();
            int LineNumber=e.getCause().getStackTrace()[i].getLineNumber();
            errorMessage=errorMessage+"\n\t---"+className+"."+MethodName+",\tline"+LineNumber;
        }
        logResult(errorMessage,log_path,log_name+"_exception");
    }
    //日志根目录
    public static String getBasePath(){
        String s=Thread.currentThread().getContextClassLoader().getResource("").getPath();
        s=s.substring(0,s.indexOf("WEB_INF"))+"log"+ File.separator;
        return s;
    }

    /**
     * 写日志
     * @param sWord 写入日志的内容
     */
    public static void logResult(String sWord){
        runWrite(sWord,log_path,log_name);
    }

    public static void logResult(String sWord,String log_Path,String log_Name){
        FileWriter writer=null;
        try {
            File dir=new File(log_Path);
            if (!dir.exists()){
                dir.mkdir();
            }
            String dt=new SimpleDateFormat("yyyy-MM-dd HH:mmss.SSS").format(new Date());
            File f=new File(log_Path+log_Name+"_"+new SimpleDateFormat(update_hz).format(new Date())+".txt");
            if (!f.exists()){
                f.createNewFile();
                sWord="AllException 日志\r\n"+"["+dt+"]\t"+sWord;
            }else {
                long logSize=f.length();

                if (logSize>=max_long_siaz){
                    String backLogName=log_Path+log_Name+new SimpleDateFormat("_yyyy-MM-dd.HHmmss.SSS").
                            format(new Date())+".txt";
                    f.renameTo(new File(backLogName));
                }
            }
            writer=new FileWriter(f,true);
            writer.write("["+dt+"]\t"+sWord+"\r\n");
            if(console_out){
                System.out.println("["+dt+"]\t"+sWord);
            }
        }catch (Exception e){
            System.out.println("记录日志异常："+e.toString());
            e.printStackTrace();
        }finally {
            if (writer!=null){
                try {
                    writer.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public static void runWrite(final String sWord,final String log_Path,final String log_Name){
        new Thread(){
            @Override
            public void run(){
                logResult(sWord,log_Path,log_Name);
            }
        }.start();
    }

    public static void main(String[] args) {
        for (int i=0;i<100;i++){
            error(""+i);
        }
    }
}
