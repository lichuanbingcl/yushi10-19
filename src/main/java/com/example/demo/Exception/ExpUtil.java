package com.example.demo.Exception;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ForkJoinPool;

public class ExpUtil {
    public static String rootPath="E:\\宇石\\yushi10-19\\src\\main\\java\\com\\example\\demo\\Exception\\log\\";

    public static void writeMsgToFile(String msg){
        delOldFile();

        FileWriter fileWriter=null;
        try{
            fileWriter =new FileWriter(getFileName(),true);
            Date today=new Date();
            String time=String.valueOf(today.getHours())+":"+
                    String.valueOf(today.getMinutes())+" "+String.valueOf(today.getSeconds());
            fileWriter.write("#"+time+"#["+msg+"]"+"\r\n");
            fileWriter.flush();
        }catch (IOException e){
            System.out.println("###写日志到文件异常###>>>"+e.getMessage());
            e.printStackTrace();
        }finally {
            try {
                fileWriter.close();
            }catch (IOException e){
                System.out.println("###关闭写日志的流异常###>>>"+e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private static void delOldFile(){
        Date today=new Date();
        int month=today.getMonth()+1;
        month=month-2;
        if (month==-1) {
            month=11;
        }
        if (month==0) {
            month=12;
        }
        String delPath=rootPath+String.valueOf(month)+"\\";
        File folder=new File(delPath);
        if (folder.exists()){
            File[] files=folder.listFiles();
            for (int i=0;i<files.length;i++){
                files[i].delete();
            }
        }
    }

    private static String getFileName(){
        Date today=new Date();
        String fileName=String.valueOf((today.getYear()+1900))+
                String.valueOf((today.getMonth()+1))+
                String.valueOf(today.getDate())+".log";

        File folder=new File(rootPath+String.valueOf((today.getMonth()+1))+"\\");
        if (!folder.exists()){
            folder.mkdir();
        }

        File file=new File(fileName);
        if (!file.exists()){
            try {
                file.createNewFile();
            }catch (IOException e){
                System.out.println("###新建日志异常###>>>"+e.getMessage());
                e.printStackTrace();
            }
        }
        fileName=rootPath+String.valueOf((today.getMonth()+1))+"\\"+fileName;

        return fileName;
    }

    public static void main(String[] args) {
        String testString="写日志：";
        writeMsgToFile(testString);
    }
}
