package com.example.demo.Exception;

import java.io.IOException;

public class AutoCloseRes implements AutoCloseable{

    private String resname;
    private int counter;

    public AutoCloseRes(String name){this.resname=name;}

    public String read() throws IOException{
        counter++;
        if (Math.random()>0.1){
            return "you get lucky to read from "+resname+" for "+counter+" times...";
        }else {
            throw new IOException("不存在");
        }
    }
    @Override
    public void close() throws Exception {
        System.out.println("资源释放了"+resname);
    }
}
