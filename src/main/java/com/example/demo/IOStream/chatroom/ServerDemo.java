package com.example.demo.IOStream.chatroom;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ServerDemo {

    public static void main(String[] args) {
        in();
    }
    public static final int SERVER_PORT=100;

    public static void in() {
        try (ServerSocket serverSocket=new ServerSocket(SERVER_PORT);
             Socket socket=serverSocket.accept();
             ){
            MyChat myChat=new MyChat("客户端","你已经连接",socket);
            myChat.chatting();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
