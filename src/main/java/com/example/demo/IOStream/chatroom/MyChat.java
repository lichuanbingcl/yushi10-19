package com.example.demo.IOStream.chatroom;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class MyChat {
    public static final int SERVER_PORT=100;

    private String from;
    private String greeting;
    private Socket socket;

    public MyChat(String from,String greeting,Socket socket){
        this.from=from;
        this.greeting=greeting;
        this.socket=socket;
    }

    public void chatting(){
        Scanner in=new Scanner(System.in);
        try (
                BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream(),
                        StandardCharsets.UTF_8));
                PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),
                        StandardCharsets.UTF_8));
        ){
            if (greeting!=null){
                pw.println(greeting);
                pw.flush();
            }
//            OutputStream os=socket.getOutputStream();
//            InputStream is=socket.getInputStream();
//            System.out.println("和客户端建立连接"+socket.getRemoteSocketAddress());
            while (true){
                String line=br.readLine();
                if (line.equalsIgnoreCase("BYE")){
                    pw.println("BYE");
                    break;
                }else {
                    System.out.println(from+" 说:"+line);
                    String myword=in.nextLine();
                    pw.println(myword);
                    pw.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
