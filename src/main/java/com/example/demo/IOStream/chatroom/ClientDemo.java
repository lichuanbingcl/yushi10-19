package com.example.demo.IOStream.chatroom;

import java.io.IOException;
import java.net.Socket;

import static com.example.demo.IOStream.chatroom.ServerDemo.SERVER_PORT;

public class ClientDemo {

    public static void main(String[] args) {
        out();
    }

    public static void out(){
        try (
                Socket socket=new Socket("127.0.0.1",SERVER_PORT);
        ){
            MyChat myChat=new MyChat("服务器端",null,socket);
            myChat.chatting();
           // System.out.println("客户端连接到"+socket.getRemoteSocketAddress());
//            OutputStream os=socket.getOutputStream();
//            byte[] b=new byte[]{1,2};
//            os.write(b,5,5);
//            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
