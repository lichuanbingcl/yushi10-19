package com.example.demo.IOStream;

import javafx.print.Printer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class WriteToFile {

    public static final Scanner in=new Scanner(System.in);

    public static void writeToFiles(File targetFile)throws IOException {
        //使用try来关闭传输
        try (
            //建立传输流管道,传输字节流
            FileOutputStream fileOutputStream=new FileOutputStream(targetFile);
            //建立传输的方式，指定字符集
            OutputStreamWriter outputStream=new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            //帮助输出字符串
            PrintWriter printWriter=new PrintWriter(outputStream);
        ){
            System.out.println("输入的内容会实时写入文件，输入空行结束");
            while (true){
                String lineToWrite=in.nextLine().trim();
                System.out.println("输入内容："+lineToWrite);
                if (lineToWrite.trim().isEmpty()){
                System.out.println("输入结束");
                break;
            }else {
                printWriter.print(lineToWrite);
                printWriter.flush();
            }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static File createFile()throws IOException{
        System.out.println("请输入文件名：");
        String fileName=in.nextLine().trim();
        File file=new File("."+File.separator+fileName+".txt");
        if (file.isFile()){
            System.out.println("目标文件已存在，删除"+file.delete());
        }
        System.out.println(file.createNewFile());
        return file;
    }


    public static void main(String[] args) throws IOException {
        File targetFile=createFile();
        writeToFiles(targetFile);
        System.out.println("程序结束");
    }
}
