package com.example.demo.IOStream;

import javafx.scene.chart.BubbleChart;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ReadFromFile {
    private static final String TEST_FILE_NAME="记录.txt";

    public static void classicWay(File soueceFile){
        System.out.println("经典—————————————————");
        try(
                FileInputStream fileInputStream=new FileInputStream(soueceFile);
                InputStreamReader inputStreamReader=new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
                BufferedReader reader=new BufferedReader(inputStreamReader);
        ){
            String line=null;
            while ((line=reader.readLine())!=null){
                System.out.println(line.trim().toUpperCase());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void coolWay(File sourceFile){
        System.out.println("更cool的方法-----");
        try (
                FileInputStream fileInputStream=new FileInputStream("."+File.separator+TEST_FILE_NAME);
                InputStreamReader inputStreamReader=new InputStreamReader(fileInputStream,StandardCharsets.UTF_8);
                BufferedReader reader=new BufferedReader(inputStreamReader);
        ){
            reader.lines().map(String::trim).map(String::toUpperCase).forEach(System.out::println);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        File sourceFile=new File("."+File.separator+TEST_FILE_NAME);
        classicWay(sourceFile);
        coolWay(sourceFile);
    }


}
