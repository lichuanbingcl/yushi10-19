package com.example.demo.File;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BFRead {
    public static void main(String[] args) throws IOException {

        char c='A';
        System.out.write(c);
        System.out.write('\n');
        System.out.write(c);
        System.out.write('\n');
    }

    private static void simpleReadLine() throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String s=null;
        System.out.println("请输入字符串，end结束");
        do {
            s=br.readLine();
            System.out.println(s);
        }while (!s.equalsIgnoreCase("end"));
    }

    private static void simpleRead() throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        char c;
        System.out.println("请输入字符，q退出");
        do {
            c=(char) br.read();
            System.out.print(c);
        }while (c!='q');
    }
}
