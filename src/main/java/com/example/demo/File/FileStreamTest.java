package com.example.demo.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileStreamTest {
    public static void main(String[] args) {
        try {
            byte[] bwriter={11,2,3,6,33};
            OutputStream os=new FileOutputStream("text.txt");
            for (int i=0;i<bwriter.length;i++){
                os.write(bwriter[i]);
            }
            os.close();

            InputStream is=new FileInputStream("text.txt");
            int size=is.available();
            for (int i=0;i<size;i++){
                System.out.println(is.read()+" ");
            }
            is.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
