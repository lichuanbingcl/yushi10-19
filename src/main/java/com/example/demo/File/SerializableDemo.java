package com.example.demo.File;

import java.io.*;

public class SerializableDemo {
    public static void main(String[] args) throws FileNotFoundException {
        Hero h=new Hero();
        h.name="didi";
        h.hp=300;

        File file=new File("./a.txt");
        try (
                FileOutputStream fos=new FileOutputStream(file);
                ObjectOutputStream oos=new ObjectOutputStream(fos);
                FileInputStream fis=new FileInputStream(file);
                ObjectInputStream ois=new ObjectInputStream(fis);
        ){
            oos.writeObject(h);
            Hero h2=(Hero)ois.readObject();
            System.out.println(h2.hp);
            System.out.println(h2.name);
        }catch (IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

class Hero implements Serializable{
    private static final int serializableID=1;
    public String name;
    public int hp;
}
