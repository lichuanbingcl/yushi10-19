package com.example.demo.File;

import java.io.File;

public class DeleteDir {
    public static void main(String[] args) {
        File folder=new File("/tmp/java.txt");
        deleteDirs(folder);
    }

    private static void deleteDirs(File folder){
        File[] files=folder.listFiles();
        if (files!=null){
            for (File f:files){
                if (f.isDirectory()){
                    deleteDirs(f);
                }else {
                    f.delete();
                }
            }
        }
        System.out.println(folder.delete());
    }
}
