package com.example.demo.File;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CreateFileApp {

    private static final String ROOT="."+File.separator;
    private static Scanner in=new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        File dir=craeteDirs();
        File newDir=renameDir(dir);
        if (newDir==null) {
            return;
        }

        String fileName=craeteFiles(newDir);

        String fileNewName=renameFiles(newDir,fileName);

        deleteFiles(newDir,fileNewName);

    }

    public static File renameDir(File dir){
        System.out.println("请输入重命名的文件名：");
        String newDirName=in.nextLine().trim();
        File file=new File(dir.getParentFile(),newDirName);
        boolean renameSuccess=dir.renameTo(file);

        if (renameSuccess){
            System.out.println("改名成功"+newDirName);
        }else {
            System.out.println("改名失败"+newDirName);
            return null;
        }
        return file;
    }

    private static String renameFiles(File newDir,String fileName){
        System.out.println("请输入新的文件名前缀：");
        String fileNewName=in.nextLine().trim();

        for (int i=0;i<20;i++){
            File file=new File(newDir,fileName+i+".txt");
            File file1=new File(newDir,fileNewName+i+".txt");
            System.out.println("重命名："+file.getName()+file.renameTo(file1));
        }
        return fileNewName;
    }


    private static String craeteFiles(File newDir) throws IOException {
        System.out.println("请输入文件前缀：");
        String fileName=in.nextLine().trim();
        for (int i=0;i<20;i++){
            File file=new File(newDir,fileName+i+".txt");
            System.out.println("创建文件："+file.getName()+file.createNewFile());
        }
        return fileName;
    }

    private static File craeteDirs(){
        List<String> list=new ArrayList<>();
        boolean flag=true;
        while (flag){
            System.out.println("请输入文件路径：");
            String path=in.nextLine();
            while (path.isEmpty()){
                break;
            }
            list.add(path);
        }
        return craeteDir(list.toArray(new String[0]));
    }

    private static File craeteDir(String... restPaths){
        String rest=joinRestDir(restPaths);
        System.out.println("将在："+ROOT+" 下创建："+rest);
        File dir=new File(ROOT,rest);
        if (dir.exists()&&dir.isDirectory()){
            System.out.println("文件已存在"+dir.toString());
            return dir;
        }else {
            boolean createSuccess=dir.mkdirs();
            if (createSuccess){
                return dir;
            }else {
                throw new IllegalArgumentException("无法在:"+ROOT+" 下建立:"+rest);
            }
        }
    }

    private static void deleteFiles(File newDir,String fileNewName){
        System.out.println("删除文件？");

        boolean isdelete=in.nextBoolean();
        if (isdelete){
            for (int i=0;i<20;i++){
                File file=new File(newDir,fileNewName+i+".txt");
                System.out.println("删除"+file.delete());
            }
        }
    }

    private static String joinRestDir(String... restPaths){
        return Arrays.stream(restPaths).map(String::trim).collect(Collectors.joining(File.separator));
    }
}