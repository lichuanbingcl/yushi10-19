package com.example.demo.File;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileStreamTest2 {
    public static void main(String[] args) throws IOException {
        File file=new File("a.txt");

        FileOutputStream fos=new FileOutputStream(file);
        OutputStreamWriter writer=new OutputStreamWriter(fos,StandardCharsets.UTF_8);
        writer.append("中文输入：");
        writer.append("你好");
        writer.append("\r\n");
        writer.append("英文输入：");
        writer.append("hello");
        writer.close();
        fos.close();

        FileInputStream fis=new FileInputStream(file);
        InputStreamReader reader=new InputStreamReader(fis,StandardCharsets.UTF_8);
        StringBuffer sb=new StringBuffer();

        while (reader.ready()){
            sb.append((char) reader.read());
        }
        System.out.println(sb.toString());
        reader.close();
        fis.close();
    }
}
